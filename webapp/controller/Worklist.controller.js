sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function(BaseController, JSONModel, formatter, Fragment, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend("com.arete.zwmuibcrpt.controller.Worklist", {

        formatter: formatter,

        onInit: function() {
            var oViewModel,
                demLogoImage = jQuery.sap.getModulePath("com.arete.zwmuibcrpt");

            oViewModel = new JSONModel({
                worklistTableTitle: this.getResourceBundle().getText("worklistTableTitle"),
                worklistTableTitleBarkod: this.getResourceBundle().getText("worklistTableTitleBarkod"),
                Barcode: "",
                DemLogo: demLogoImage,
                Stocks: [],
                TipKontrol: "",
                BarcodeTypeControl: true,
                BarkodTablo: [],
                BarkodModel: [],
                NedenSet: [],
                NedenId: "",
                SelectControl: "",
                MengeToplam:0,


            });
            this.setModel(oViewModel, "worklistView");


            this.onGetSearchHelp();


        },
        onBarkodGiris: function() {
            var that = this,
                oViewModel = this.getModel("worklistView"),
                oDataModel = this.getModel();
            // var barkodInput = this.byId("barkodInputID").getValue();
            var barkodInput = oViewModel.getProperty("/Barcode"),
                mIslem = "Y";
            oDataModel.read(
                "/BarcodeSet(Islem='" + mIslem + "',Barkod='" + barkodInput + "')", {
                    method: "GET",
                    success: function(data) {


                        var tmpArr = [];
                        tmpArr.push(data);
                        oViewModel.setProperty("/BarkodTablo", tmpArr); //bu kısımda barkodTablo modelimi header bilgimle doldurdum.
                    },
                    error: function() {
                        oViewModel.setProperty("/Barcode", "");

                    },
                }
            );
            var wmStok = true,
                wmStokFilter = [];
            wmStokFilter.push(new Filter("WmStok", FilterOperator.EQ, wmStok));
            var that = this,
                oViewModel = this.getModel("worklistView");


            this.getModel("worklistView").setProperty("/BarcodeType", "");
            oDataModel.read(
                "/BarcodeSet(Islem='" + mIslem + "',Barkod='" + barkodInput + "')/BarcodeToBarcodeStock", {
                    method: "GET",
                    filters: wmStokFilter,
                    success: function(data) {
                        debugger;
                     var arr=data.results;
                        const merged = arr.reduce((r, {
                            Matnr,
                            Charg,
                            Bestq,
                            Vfdat,
                            ...rest
                        }) => {
                            const key = `${Matnr}-${Charg}-${Bestq}`;
                            r[key] = r[key] || {
                                Matnr,
                                Charg,
                                Bestq,
                                Vfdat,
                                grupArr: []
                            };
                            r[key]["grupArr"].push(rest)
                            return r;
                        }, {})

                        const grupTable = Object.values(merged);
                        debugger;
                        var sum=0;
                        for(var i=0;i<grupTable.length;i++)
                        {
                            debugger;
                            for(var j=0;j<grupTable[i].grupArr.length;j++)
                            {
                                sum += parseInt(grupTable[i].grupArr[j].Menge);
                            }
                            
                        }
                        oViewModel.setProperty("/MengeToplam",sum);
                        oViewModel.setProperty("/Stocks", grupTable); //aynıysa toplatıp bastım değilse direkt bastım tempArr ı.


                        var barkodData = oViewModel.getProperty("/BarkodTablo"), //yukarıda barkodTablo modelimi headerle doldurmuştum.barkodData ya taşıdım.
                            arrBarkodData = [];
                        for (var i = 0; i < barkodData.length; i++) {
                            if (barkodData[i].BarkodTipi === "TAP") {
                                arrBarkodData.push({
                                    Barkod: barkodData[i].Barkod, //header bilgilerimi bastığım alan
                                    BarkodTipi: "TAP",
                                    PaletId: barkodData[i].PaletId
                                });
                                for (var k = 0; k < data.results.length; k++) {
                                    arrBarkodData.push({
                                        Barkod: data.results[i].TabId, //barkodtobarkodstocktan gelenleri bastığım alan.
                                        BarkodTipi: "TAB",
                                        PaletId: data.results[i].PaletId
                                    });
                                }

                            } else {
                                arrBarkodData.push({
                                    Barkod: barkodData[i].Barkod, //burada direkt header bilgilerimi bastım.
                                    BarkodTipi: barkodData[i].BarkodTipi,
                                    PaletId: barkodData[i].PaletId
                                });
                            }


                        }
                        oViewModel.setProperty("/BarkodModel", arrBarkodData); //seçim yapıldı tabloya bastım.BarkodModel tanımladım 2.barkod tableımın itemı olarak değiştim.


                    },
                    error: function() {
                        oViewModel.setProperty("/Barcode", "");
                    },
                }

            );
        },
        selectionChangeHandler: function(oEvent) {
            debugger;
            var oViewModel = this.getModel("worklistView"),
                selectControl = oEvent.getParameter("selected").toString();
            if (selectControl === "false") {
                selectControl = 0;
            }
            oViewModel.setProperty("/SelectControl", selectControl);




        },

        onPressClear: function(oEvent) {
            var that = this,
            oViewModel = this.getModel("worklistView"),
            oTable = this.getView().byId("barkodTableId");
            oTable.removeSelections();
            oViewModel.setProperty("/Stocks", []);
            oViewModel.setProperty("/BarkodModel", []);
            oViewModel.setProperty("/Barcode", "");
            oViewModel.setProperty("/SelectControl", "");
            oViewModel.setProperty("/NedenId", "");
            this.byId("idinputYazdirma").setValue("");



        },
        onUpdateFinished: function(oEvent) {
            // update the worklist's object counter after the table update
            var sTitle,
                oTable = oEvent.getSource(),
                iTotalItems = oEvent.getParameter("total");
            // only update the counter if the length is final and
            // the table is not empty
            if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
                sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
            } else {
                sTitle = this.getResourceBundle().getText("worklistTableTitle");
            }
            this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
        },
        onUpdateFinishedBarkod: function(oEvent) {
            var sTitle,
                oTable = oEvent.getSource(),
                iTotalItems = oEvent.getParameter("total");
            // only update the counter if the length is final and
            // the table is not empty
            if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
                sTitle = this.getResourceBundle().getText("worklistTableTitleCountBarkod", [iTotalItems]);
            } else {
                sTitle = this.getResourceBundle().getText("worklistTableTitleBarkod");
            }
            this.getModel("worklistView").setProperty("/worklistTableTitleBarkod", sTitle);
        },

        onRefresh: function() {
            var oTable = this.byId("table");
            oTable.getBinding("items").refresh();
        },
        onGetSearchHelp: function() {
            var oViewModel = this.getModel("worklistView"),

                oModel = this.getOwnerComponent().getModel();
            oModel.read(
                "/ShPrintReasonSet", {
                    method: "GET",
                    success: function(data) {
                        oViewModel.setProperty("/NedenSet", data.results);
                    },
                    error: function() {

                    },
                }
            );

        },
        onYazdirmaNedeni: function(oEvent) {
            var oViewModel = this.getModel("worklistView"),
                sInputValue = oEvent.getSource().getValue(),
                oView = this.getView();

            if (!this._pValueHelpDialog) {
                this._pValueHelpDialog = Fragment.load({
                    id: oView.getId(),
                    name: "com.arete.zwmuibcrpt.view.ValueHelpDialog",
                    controller: this
                }).then(function(oDialog) {
                    oView.addDependent(oDialog);
                    return oDialog;
                });
            }
            this._pValueHelpDialog.then(function(oDialog) {

                oDialog.open(sInputValue);
            });
        },
        onCloseDialog: function(oEvent) {
            var oViewModel = this.getModel("worklistView"),
                oSelectedItem = oEvent.getParameter("selectedItem");
            oEvent.getSource().getBinding("items").filter([]);

            if (!oSelectedItem) {
                return;
            }
            this.byId("idinputYazdirma").setValue(oSelectedItem.getTitle());
            var nedenId = oSelectedItem.getInfo();
            oViewModel.setProperty("/NedenId", nedenId);

        },
        onInputChange: function(oEvent) {
            debugger;
            var oViewModel = this.getModel("worklistView"),
                oSelectedItem = oEvent.getParameter("selectedItem");
            var nedenIdguncel = oSelectedItem;
            oViewModel.setProperty("/NedenId", nedenIdguncel);
        },
        onValueHelpSearch: function(oEvent) {
            var sValue = oEvent.getParameter("value"),
                oFilter = new Filter("NedenTxt", FilterOperator.Contains, sValue);

            oEvent.getSource().getBinding("items").filter([oFilter]);
        },
        onYazdirPress: function(oEvent) {
            var arr = [],
                that = this,
                oViewModel = this.getModel("worklistView"),
                //yazdırma nedeni ve barkodlar seçilmeden tıklanamayacak.
                oTable = this.getView().byId("barkodTableId"),
                oContext = oTable.getSelectedContexts();
            for (var i = 0; i < oContext.length; i++) {
                arr.push(oViewModel.getProperty("/BarkodModel")[i]);

            }
            var oPrintReason = oViewModel.getProperty("/NedenId"),
                oViewModel = this.getModel("worklistView"),
                oData = {};
            oData.Islem = "Y";
            oData.PrintReason = parseInt(oPrintReason);
            oData.ProcessToBarcode = arr;
            var oDataModel = this.getModel();
            oDataModel.create(
                "/ProcessSet", oData, {
                    success: function(data) {

                        that.onPressClear();

                    },
                    error: function() {

                    },
                }
            );
        }




    });
});